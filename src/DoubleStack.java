import java.util.*;
import java.util.regex.Pattern;

import static org.junit.Assert.assertTrue;

/**
 * Stack manipulation.
 *
 * @since 1.8
 */
public class DoubleStack {

   public static void main(String[] argum) {
   }

   private LinkedList<Double> list;

   DoubleStack() {
      list = new LinkedList<>();
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      DoubleStack clone = new DoubleStack();
      for (int i = (int) size() - 1; i >= 0; i--) {
         clone.push(get(i));
      }
      return clone;
   }

   public boolean stEmpty() {
      return list.isEmpty();
   }

   public void push(double a) {
      list.push(a);
   }

   public double pop() {
      if (size() < 1) {
         throw new IndexOutOfBoundsException("Stack underflow");
      }
      return list.pop();
   }

   public void op(String s) {
      if (size() < 1) {
         throw new IllegalArgumentException("not enough numbers to execute " + s);
      }
      double secondMember = pop();
      double firstMember = pop();

      switch (s) {

         //It is intentional.
         case "–":
         case "-":
            push(firstMember - secondMember);
            break;
         case "*":
            push(firstMember * secondMember);
            break;
         case "+":
            push(firstMember + secondMember);
            break;
         case "/":
            push(firstMember / secondMember);
            break;
         default:
            throw new IllegalArgumentException("Symbol '" + s + "' is not part of accepted symbols.");
      }
   }

   public double tos() {
      if (size() < 1) {
         throw new IndexOutOfBoundsException("Stack underflow");
      }
      return list.getFirst();
   }

   //https://stackoverflow.com/questions/7347442/how-to-convert-java-lang-object-to-arraylist
   @Override
   public boolean equals(Object o) {
      if (!(o instanceof DoubleStack)) {
         return false;
      }
      return list.equals(((DoubleStack) o).list);
   }

   //https://www.baeldung.com/java-list-to-string
   @Override
   public String toString() {
      StringBuffer result = new StringBuffer();
      for (int i = (int) size() - 1; i >= 0; i--) {
         result.append(get(i));
         result.append(" ");
      }
      return result.toString();
   }


   public double size() {
      return list.size();
   }

   public double get(int i) {
      return list.get(i);
   }

   private void swap() {
      if (size() < 1) {
         throw new RuntimeException("Not enough members for swap");
      }
      double first = pop();
      double second = pop();
      push(first);
      push(second);
   }

   private void dup() {
      if (size() < 1) {
         throw new RuntimeException("Not enough members for duplicate");
      }
      double first = pop();
      push(first);
      push(first);
   }

   private void rot() {
      if (size() < 3) {
         throw new RuntimeException("Not enough members for rotation");
      }
      double first = pop();
      double second = pop();
      double third = pop();
      push(second);
      push(first);
      push(third);
   }


   //detects if interpreter has more numbers than operators
   private double isReady() {
      double result = pop();
      if (!stEmpty()) {
         throw new RuntimeException(" contains more numbers than necessary");
      } else {
         return result;
      }
   }

   //String cleaner and empty string detector for interpreter
   private String stringCleaner(String pol) {
      String cleaned = pol.replace("\t", "");
      cleaned = cleaned.replace("\n", "");
      cleaned = cleaned.trim().replaceAll(" +", " ");

      if (cleaned.equals("")) {
         throw new RuntimeException("String is either null, empty or full of whitespaces");
      }
      return cleaned;
   }

   //https://www.baeldung.com/java-check-string-number
   public static boolean isNumeric(String strNum) {
      if (strNum == null) {
         return false;
      }
      try {
         double d = Double.parseDouble(strNum);
      } catch (NumberFormatException nfe) {
         return false;
      }
      return true;
   }


   public static double interpret(String pol) {
      try {
         DoubleStack stack = new DoubleStack();
         String modified = stack.stringCleaner(pol);
         List<String> raw = Arrays.asList(modified.split("\\s* \\s*"));
         for (String s : raw) {
            if (isNumeric(s) || s.contains(".")) {
               stack.push(Double.parseDouble(s));
            } else if (s.equals("SWAP")) {
               stack.swap();
            } else if (s.equals("DUP")) {
               stack.dup();
            } else if (s.equals("ROT")) {
               stack.rot();
            } else {
               stack.op(s);
            }
         }
         return stack.isReady();
      } catch (IndexOutOfBoundsException e) {
         throw new RuntimeException("String " + pol + " Has more numbers than operations");
      } catch (RuntimeException e) {
         throw new RuntimeException("String '" + pol + "' had problem, was caused by: " + e.getMessage());
      }
   }
}